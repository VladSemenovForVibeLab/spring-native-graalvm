# Customer приложение

Customer приложение, основанное на следующих зависимостях:

```xml
<dependencies>
    <dependency>
        <groupId>io.r2dbc</groupId>
        <artifactId>r2dbc-h2</artifactId>
        <scope>runtime</scope>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-data-r2dbc</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-webflux</artifactId>
    </dependency>
    <dependency>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok</artifactId>
        <optional>true</optional>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-test</artifactId>
        <scope>test</scope>
    </dependency>
    <dependency>
        <groupId>io.projectreactor</groupId>
        <artifactId>reactor-test</artifactId>
        <scope>test</scope>
    </dependency>
</dependencies>
```

## Сборка

Используется Maven для сборки проекта.

```xml
<build>
    <plugins>
        <plugin>
            <groupId>org.graalvm.buildtools</groupId>
            <artifactId>native-maven-plugin</artifactId>
        </plugin>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
            <configuration>
                <excludes>
                    <exclude>
                        <groupId>org.projectlombok</groupId>
                        <artifactId>lombok</artifactId>
                    </exclude>
                </excludes>
            </configuration>
        </plugin>
    </plugins>
</build>
```

## Описание

Customer приложение является примером приложения, использующего Spring Boot, R2DBC для взаимодействия с базой данных H2 и WebFlux для создания реактивного API. Оно также использует Lombok для упрощения написания кода и включает модульные тесты для проверки функциональности.

## Установка и запуск

1. Клонируйте репозиторий приложения.
2. В корневом каталоге проекта выполните команду `mvn package` для сборки приложения.
3. Запустите приложение, выполнив команду `java -jar target/customer-<version>.jar`, где `<version>` - версия приложения.
4. Приложение будет доступно по адресу `http://localhost:8080`.

## Документация API

| Метод | Путь | Описание |
|-------|------|----------|
| GET | `/customers` | Получить список клиентов |
