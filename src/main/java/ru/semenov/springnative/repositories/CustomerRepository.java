package ru.semenov.springnative.repositories;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import ru.semenov.springnative.models.Customer;

@Repository
public interface CustomerRepository extends ReactiveCrudRepository<Customer,Integer> {

}
