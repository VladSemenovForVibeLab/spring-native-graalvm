package ru.semenov.springnative.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import ru.semenov.springnative.models.Customer;
import ru.semenov.springnative.repositories.CustomerRepository;

@Service
public class CustomerService {
    @Autowired
    CustomerRepository customerRepository;
    @Bean
    ApplicationListener<ApplicationReadyEvent> applicationReadyEventApplicationListener(){
        return new ApplicationListener<ApplicationReadyEvent>() {
            @Override
            public void onApplicationEvent(ApplicationReadyEvent event) {
                Flux.just("Daniel","Peter","Mary","Terryn")
                        .map(firstname->new Customer(null,firstname,null,null))
                        .flatMap(customerRepository::save)
                        .subscribe(System.out::println);
            }
        };
    }
    public Flux<Customer> fetchAllCustomers(){
        return customerRepository.findAll();
    }
}
